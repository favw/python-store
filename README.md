# python-store

A barebones Python app, which can be deployed by Auto DevOps.

## Running Locally

Make sure you have Python [installed properly](http://install.python-guide.org).

```sh
$ git clone git@gitlab.com:danielgruesso/python-store.git
$ cd python-getting-started
$ pip install -r requirements.txt
$ createdb python_getting_started
$ foreman run python manage.py migrate
$ python manage.py collectstatic
$ foreman start web
```

Your app should now be running on [localhost:5000](http://localhost:5000/).
